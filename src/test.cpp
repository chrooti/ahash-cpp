#include "../ahash.hpp"

#include <utter/utter.hpp>

int test_uint() {
    constexpr uint64_t payload = 333;
    constexpr uint64_t expected_hash = 661414113657319803;

    ahash::RandomState const random_state = ahash::random_state::with_seeds(1, 2, 3, 4);
    ahash::Hasher hasher = ahash::from_random_state(random_state);

    ahash::write_u64(hasher, payload);
    const uint64_t hash = ahash::finish(hasher);

    if (expected_hash != hash) {
        utter::to(
            utter::STDERR,
            "[ERROR] expected hash: ",
            expected_hash,
            " obtained hash: ",
            hash,
            " payload: ",
            payload,
            "\n"
        );
        return 1;
    }

    return 0;
}

int test_bytes() {
    constexpr char payload[] = "hello, world";
    const size_t payload_size = sizeof(payload) - 1;
    constexpr uint64_t expected_hash = 12255089999572761645U;

    ahash::RandomState const random_state = ahash::random_state::with_seeds(1, 2, 3, 4);
    ahash::Hasher hasher = ahash::from_random_state(random_state);

    ahash::write(hasher, payload, payload_size);
    const uint64_t hash = ahash::finish(hasher);

    if (expected_hash != hash) {
        utter::to(
            utter::STDERR,
            "[ERROR] expected hash: ",
            expected_hash,
            " obtained hash: ",
            hash,
            " payload: ",
            payload,
            "\n"
        );
        return 1;
    }

    return 0;
}

int main() {
    int ret = 0;
    ret += test_uint();
    ret += test_bytes();

    return ret;
}
