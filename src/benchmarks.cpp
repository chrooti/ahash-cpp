#include "../ahash.hpp"

// wrapper for SMHasher
uint64_t ahash64(char* buf, uint64_t len, uint64_t seed) {
    ahash::RandomState const state =
        ahash::random_state::with_seeds(seed, seed, seed, seed);
    ahash::Hasher hasher = ahash::from_random_state(state);
    ahash::write(hasher, buf, len);
    return ahash::finish(hasher);
}
