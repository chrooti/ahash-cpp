#ifndef UTTER_H
#define UTTER_H

#include <stddef.h>
#include <stdint.h>

#include <type_traits>
#include <utility>

/* OS macros */

#if __STDC_HOSTED__ && defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
#include <string.h>
#include <sys/uio.h>

#else
#if INTPTR_MAX == INT64_MAX
using ssize_t = int64_t;
#elif INTPTR_MAX == INT32_MAX
using ssize_t = int32_t;
#else
#error "Cannot detect pointer size"
#endif

struct iovec;
void* memcpy(void* destination, const void* source, size_t num);
ssize_t writev(int fd, const struct iovec* iovec, int count);

#endif

/* Compiler macros */

namespace utter::detail {

#if defined(__clang__) || defined(__GNUC__)
#define UTTER_INLINE static inline __attribute__((__always_inline__, __unused__))

UTTER_INLINE constexpr unsigned int clz(size_t val) {
    return static_cast<unsigned int>(__builtin_clzll(val));
}

#else
// the following line makes clang-format 14 crash
// clang-format off
#define UTTER_INLINE static inline
// clang-format on

UTTER_INLINE constexpr unsigned int clz(size_t val);

#endif

} // namespace utter::detail

/* Buffers */

namespace utter {

template<size_t n>
struct Buffer {
    static constexpr size_t SIZE = n;

    size_t len;
    char data[n];
};

struct BufferView {
    size_t len;
    const char* data;
};

} // namespace utter

/* General purpose utilities */

namespace utter::detail {

template<size_t n>
static constexpr size_t strlen(const char (&)[n]) {
    return n - 1;
}

} // namespace utter::detail

/* Conversion routines */

namespace utter {

enum : int {
    // first 12 bits are reserved for fill size

    // add "0x", "0b" etc. prefix (ignored when printing decimal)
    UTTER_CONV_NO_PREFIX = 0x1000,
};

} // namespace utter

namespace utter::detail {

UTTER_INLINE constexpr unsigned int count_binary_digits(size_t val) {
    // NOTE: __builtin_clz(x) is undefined for x == 0
    return sizeof(size_t) * 8 - ::utter::detail::clz(val);
}

// most arches have 2 return registers, so these  still fits

struct ConversionArgs {
    bool wants_prefix;
    unsigned int fill;
};

struct ConversionResult {
    char* buf_end;
    unsigned int len;
};

template<typename IntegerConversionStrategy>
struct IntegerConverter {
    static constexpr unsigned int PREFIX_LEN =
        ::utter::detail::strlen(IntegerConversionStrategy::PREFIX);

    static constexpr unsigned int MAX_UNSIGNED_LEN =
        IntegerConversionStrategy::unsigned_len(UINT64_MAX) + PREFIX_LEN;
    static constexpr unsigned int MAX_SIGNED_LEN =
        IntegerConversionStrategy::unsigned_len(-static_cast<size_t>(INT64_MIN)) + 1
        + PREFIX_LEN;

    static constexpr unsigned int MAX_LEN =
        MAX_UNSIGNED_LEN > MAX_SIGNED_LEN ? MAX_UNSIGNED_LEN : MAX_SIGNED_LEN;

    /* Helpers, these are istantiated later with great care to avoid code bloat */

    UTTER_INLINE constexpr ConversionArgs process_flags(int flags) {
        // first 12 bits are reserved for fill count
        unsigned int fill = flags & 0xFFF;
        fill = fill > MAX_UNSIGNED_LEN ? MAX_UNSIGNED_LEN : fill;

        return {
            .wants_prefix = (flags & UTTER_CONV_NO_PREFIX) == 0,
            .fill = fill,
        };
    }

    UTTER_INLINE constexpr size_t to_unsigned(ssize_t val) {
        return val > 0 ? static_cast<size_t>(val) : (~static_cast<size_t>(val) + 1);
    }

    template<typename I>

        requires requires(I) {
            std::is_unsigned_v<I>;
        }

    UTTER_INLINE constexpr unsigned int
    do_unsigned_len(I val, const ConversionArgs& args) {
        unsigned int len = val == 0 ? 1 : IntegerConversionStrategy::unsigned_len(val);

        len = args.fill > len ? args.fill : len;
        if constexpr (PREFIX_LEN != 0) {
            len += PREFIX_LEN * static_cast<unsigned int>(args.wants_prefix);
        }

        return len;
    }

    template<typename I>

        requires requires(I) {
            std::is_signed_v<I>;
        }

    UTTER_INLINE constexpr unsigned int add_signed_len(I val) {
        return static_cast<unsigned int>(val < 0);
    }

    template<typename I>

        requires requires(I) {
            std::is_unsigned_v<I>;
        }

    UTTER_INLINE constexpr char*
    do_unsigned_to_string(char* buf_end, I val, const ConversionArgs& args) {
        char* buf_start = IntegerConversionStrategy::unsigned_to_string(val, buf_end);

        for (int fill =
                 static_cast<int>(args.fill) - static_cast<int>(buf_end - buf_start);
             fill > 0;
             fill--) {
            buf_start--;
            *buf_start = '0';
        }

        if (args.wants_prefix) {
            buf_start = IntegerConversionStrategy::add_prefix(buf_start);
        }

        return buf_start;
    }

    template<typename I>

        requires requires(I) {
            std::is_signed_v<I>;
        }

    UTTER_INLINE constexpr char* add_signed_to_string(char* buf_start, I val) {
        if (val < 0) {
            buf_start--;
            *buf_start = '-';
        }

        return buf_start;
    }

    /* Actual functions, each one of these is constructed by blocks constituted of helpers above
    *  since each of these is possibly emitted as assembly the number must be kept small.
    */

    static constexpr ConversionResult
    unsigned_to_string(char* buf, size_t val, int flags) {
        const ConversionArgs args = process_flags(flags);
        const unsigned int len = do_unsigned_len(val, args);
        char* const buf_end = buf + len;
        do_unsigned_to_string(buf_end, val, args);

        return {.buf_end = buf_end, .len = len};
    }

    static constexpr ConversionResult
    signed_to_string(char* buf, ssize_t val, int flags) {
        const ConversionArgs args = process_flags(flags);
        const size_t unsigned_val = to_unsigned(val);

        const unsigned int len =
            do_unsigned_len(unsigned_val, args) + add_signed_len(val);

        char* const buf_end = buf + len;
        char* const buf_start = do_unsigned_to_string(buf_end, unsigned_val, args);
        add_signed_to_string(buf_start, val);

        return {.buf_end = buf_end, .len = len};
    }

    /* External facing API, instantiates a signed/unsigned pair of the functions above */

    template<typename I>
    UTTER_INLINE constexpr ConversionResult to_string(char* buf, I val, int flags) {
        if constexpr (std::is_unsigned_v<I>) {
            return unsigned_to_string(buf, val, flags);
        } else {
            return signed_to_string(buf, val, flags);
        }
    }

    template<typename I>
    UTTER_INLINE constexpr auto to_buffer(I val, int flags) {
        if constexpr (std::is_unsigned_v<I>) {
            Buffer<MAX_UNSIGNED_LEN> buf = {};
            const ConversionResult result = unsigned_to_string(buf.data, val, flags);
            buf.len = result.len;
            return buf;
        } else {
            Buffer<MAX_SIGNED_LEN> buf = {};
            const ConversionResult result = signed_to_string(buf.data, val, flags);
            buf.len = result.len;
            return buf;
        }
    }

    /* Like above but with constexpr, these should never be emitted as asm */

    template<auto val, int flags>
    static constexpr auto to_buffer() {
        constexpr ConversionArgs args = process_flags(flags);

        if constexpr (std::is_unsigned_v<decltype(val)>) {
            constexpr unsigned int len = do_unsigned_len(val, args);
            Buffer<len> buf = {.len = len, .data = {}};

            do_unsigned_to_string(buf.data + len, val, args);

            return buf;
        } else {
            constexpr size_t unsigned_val = to_unsigned(val);
            constexpr unsigned int len =
                do_unsigned_len(unsigned_val, args) + add_signed_len(val);
            Buffer<len> buf = {.len = len, .data = {}};

            char* const buf_end = buf.data + len;
            char* const buf_start = do_unsigned_to_string(buf_end, unsigned_val, args);
            add_signed_to_string(buf_start, val);

            return buf;
        }
    }
};

struct DecimalConversionStrategy {
    static constexpr char PREFIX[] = "";

    // 2^x has "power_of_two_digits_minus_one[x] - 1" digits in decimal
    // due to how __builtin_clz works 0 has a special val of 1
    static constexpr unsigned char POWER_OF_TWO_DIGITS_MINUS_ONE[65] = {
        // clang-format off

        // 0, 1, 2, 4, 8
        0, 0, 0, 0,

        // 16, 32, 64
        1, 1, 1,

        // 128, 256, 512
        2, 2, 2,

        // 1024, 2048, 4096, 8192
        3, 3, 3, 3,

        4, 4, 4,
        5, 5, 5,
        6, 6, 6, 6,

        7, 7, 7,
        8, 8, 8,
        9, 9, 9, 9,

        10, 10, 10,
        11, 11, 11,
        12, 12, 12, 12,

        13, 13, 13,
        14, 14, 14,
        15, 15, 15, 15,

        16, 16, 16,
        17, 17, 17,
        18, 18, 18, 18,

        19,

        // clang-format on
    };

    // 10^x = powers_of_ten[x]
    static constexpr size_t POWERS_OF_TEN[] = {
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000,
        10000000000,
        100000000000,
        1000000000000,
        10000000000000,
        100000000000000,
        1000000000000000,
        10000000000000000,
        100000000000000000,
        1000000000000000000U,
#if INTPTR_MAX > INT32_MAX
        10000000000000000000U,
#endif
    };

    // LUT of the first 100 numbers, see this as an array of 50 elements
    // with size of each element == 2 * sizeof(char)
    static constexpr char DECIMAL_LUT[] =
        "0001020304050607080910111213141516171819"
        "2021222324252627282930313233343536373839"
        "4041424344454647484950515253545556575859"
        "6061626364656667686970717273747576777879"
        "8081828384858687888990919293949596979899";

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        // inspired by https://stackoverflow.com/questions/25892665/performance-of-log10-function-returning-an-int
        // how does this work, by example:
        // - 75:
        // digits in binary = 7
        // guess[7] = 2
        // powers_of_ten[2] = 100
        // 75 < 100 -> returns 2
        // - 110:
        // digits in binary = 7
        // guess[7] = 2
        // powers_of_ten[2] = 100
        // 110 > 100 -> returns 3

        const unsigned int digits_in_binary = ::utter::detail::count_binary_digits(val);

        unsigned int digits = POWER_OF_TWO_DIGITS_MINUS_ONE[digits_in_binary];
        digits += static_cast<unsigned int>(val >= POWERS_OF_TEN[digits]);

        return digits;
    }

    UTTER_INLINE constexpr char* add_prefix(char* buf_end) {
        return buf_end;
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        // unrolling of the case > 10000
        while (val >= 10000) {
            const size_t rem = val % 10000;

            val /= 10000;

            const size_t digit_pair1 = (rem / 100) << 1;
            const size_t digit_pair2 = (rem % 100) << 1;

            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair2];
            buf_end[1] = DECIMAL_LUT[digit_pair2 + 1];

            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair1];
            buf_end[1] = DECIMAL_LUT[digit_pair1 + 1];
        }

        if (val >= 100) {
            // the index on the lut above is given by:
            // - dividing by 100 to find "the cell"
            // - multiplying by two since each cell is made by two chars
            const size_t digit_pair = (val % 100) << 1;

            val /= 100;

            // decrement the current position since we start from the end of the buffer
            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair];
            buf_end[1] = DECIMAL_LUT[digit_pair + 1];
        }

        if (val >= 10) {
            const unsigned int digit_pair = static_cast<unsigned int>(val) << 1;
            buf_end -= 2;
            buf_end[0] = DECIMAL_LUT[digit_pair];
            buf_end[1] = DECIMAL_LUT[digit_pair + 1];
        } else {
            buf_end--;

            // NOLINTNEXTLINE(cppcoreguidelines-narrowing-conversions)
            *buf_end = static_cast<char>(val) + '0';
        }

        return buf_end;
    }
};

struct HexadecimalConversionStrategy {
    static constexpr char PREFIX[] = "0x";

    static constexpr char HEX_LUT[] = "0123456789abcdef";

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        const unsigned int digits_in_binary = ::utter::detail::count_binary_digits(val);
        return digits_in_binary / 4
            + static_cast<unsigned int>((digits_in_binary % 4) != 0);
    }

    UTTER_INLINE constexpr char* add_prefix(char* buf_end) {
        buf_end--;
        *buf_end = 'x';
        buf_end--;
        *buf_end = '0';

        return buf_end;
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        while (val > 15) {
            buf_end--;
            *buf_end = HEX_LUT[val & 0xF];
            val >>= 4;
        }

        buf_end--;
        *buf_end = HEX_LUT[val & 0xF];

        return buf_end;
    }
};

struct BinaryConversionStrategy {
    static constexpr char PREFIX[] = "0b";

    UTTER_INLINE constexpr unsigned int unsigned_len(size_t val) {
        return count_binary_digits(val);
    }

    UTTER_INLINE constexpr char* add_prefix(char* buf_end) {
        buf_end--;
        *buf_end = 'b';
        buf_end--;
        *buf_end = '0';

        return buf_end;
    }

    UTTER_INLINE constexpr char* unsigned_to_string(size_t val, char* buf_end) {
        while (val > 1) {
            buf_end--;

            // NOLINTNEXTLINE(cppcoreguidelines-narrowing-conversions)
            *buf_end = (val & 0x1) + '0';

            val >>= 1;
        }

        buf_end--;

        // NOLINTNEXTLINE(cppcoreguidelines-narrowing-conversions)
        *buf_end = (val & 0x1) + '0';

        return buf_end;
    }
};

} // namespace utter::detail

namespace utter {

template<typename I>
UTTER_INLINE constexpr char* as_dec_into(I val, char* buffer, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::DecimalConversionStrategy>::to_string(val, buffer, flags);
}

template<typename I>
UTTER_INLINE constexpr auto as_dec(I val, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::DecimalConversionStrategy>::to_buffer(val, flags);
}

template<auto val, int flags = 0>
UTTER_INLINE constexpr auto as_dec() {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::DecimalConversionStrategy>::to_buffer<val, flags>();
}

template<typename I>
UTTER_INLINE constexpr char* as_hex_into(I val, char* buffer, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::HexadecimalConversionStrategy>::to_string(val, buffer, flags);
}

template<typename I>
UTTER_INLINE constexpr auto as_hex(I val, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::HexadecimalConversionStrategy>::to_buffer(val, flags);
}

template<auto val, int flags = 0>
UTTER_INLINE constexpr auto as_hex() {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::HexadecimalConversionStrategy>::to_buffer<val, flags>();
}

template<typename I>
UTTER_INLINE constexpr char* as_bin_into(I val, char* buffer, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::BinaryConversionStrategy>::to_string(val, buffer, flags);
}

template<typename I>
UTTER_INLINE constexpr auto as_bin(I val, int flags = 0) {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::BinaryConversionStrategy>::to_buffer(val, flags);
}

template<auto val, int flags = 0>
UTTER_INLINE constexpr auto as_bin() {
    return ::utter::detail::IntegerConverter<
        ::utter::detail::BinaryConversionStrategy>::to_buffer<val, flags>();
}

} // namespace utter

/* Buffer constructors */

namespace utter::detail {

template<size_t... indices>
UTTER_INLINE constexpr Buffer<sizeof...(indices)>
to_buffer(const char (&str)[sizeof...(indices) + 1], std::index_sequence<indices...>) {
    return {
        .len = sizeof...(indices),
        .data = {str[indices]...},
    };
}

template<auto val>
struct ToStatic {
    static constexpr decltype(val) STATIC_VAL = val;
};

} // namespace utter::detail

namespace utter {

template<size_t n>
UTTER_INLINE constexpr const Buffer<n>& to_buffer(const Buffer<n>& buf) {
    return buf;
}

UTTER_INLINE constexpr const BufferView& to_buffer(const BufferView& buf_view) {
    return buf_view;
}

UTTER_INLINE constexpr Buffer<1> to_buffer(const char chr) {
    return {
        .len = 1,
        .data = {chr},
    };
}

template<size_t n>
UTTER_INLINE constexpr Buffer<n - 1> to_buffer(const char (&str)[n]) {
    return ::utter::detail::to_buffer(str, std::make_index_sequence<n - 1>{});
}

template<typename I>

    requires requires(I) {
        std::is_integral_v<I> && (!std::is_same_v<I, char>);
    }

UTTER_INLINE
constexpr auto to_buffer(I val) {
    return ::utter::as_dec(val);
}

UTTER_INLINE constexpr BufferView to_buffer_view(const char* data, size_t len) {
    return {
        .len = len,
        .data = data,
    };
}

} // namespace utter

#define S(...) ::utter::detail::ToStatic<::utter::to_buffer(__VA_ARGS__)>::STATIC_VAL

namespace utter::literals {

template<char... chars>
UTTER_INLINE constexpr Buffer<sizeof...(chars)> operator""_str() {
    return {
        .len = sizeof...(chars),
        .data = {chars...},
    };
}

} // namespace utter::literals

/* Buffer utilities */

namespace utter::detail {

template<typename T>
UTTER_INLINE constexpr void into_buffer(T&&) {
}

template<typename T1, typename T2, typename... Ts>
UTTER_INLINE constexpr void into_buffer(T1&& buf1, T2&& buf2, Ts&&... bufs) {
    if (std::is_constant_evaluated()) {
        for (size_t i = 0; i < buf2.len; i++) {
            buf1.data[buf1.len + i] = buf2.data[i];
        }
    } else {
        ::memcpy(buf1.data + buf1.len, buf2.data, buf2.len);
    }

    buf1.len += buf2.len;
    ::utter::detail::into_buffer(buf1, bufs...);
}

template<typename... Ts>
UTTER_INLINE constexpr auto together(Ts&&... bufs) {
    constexpr size_t merged_buf_len = (std::remove_reference_t<Ts>::SIZE + ... + 0);
    Buffer<merged_buf_len> merged_buf = {
        .len = 0,
        .data = {},
    };

    ::utter::detail::into_buffer(merged_buf, bufs...);

    return merged_buf;
}

} // namespace utter::detail

namespace utter {

template<typename... Args>
UTTER_INLINE constexpr auto together(Args&&... args) {
    return ::utter::detail::together(::utter::to_buffer(args)...);
}

template<size_t n>
UTTER_INLINE constexpr auto repeat(char chr) {
    Buffer<n> buf = {
        .len = n,
        .data = {},
    };
    if (std::is_constant_evaluated()) {
        for (size_t i = 0; i < n; i++) {
            buf.data[i] = chr;
        }
    } else {
        ::memset(buf.data, chr, n);
    }

    return buf;
}

} // namespace utter

/* IO */

namespace utter::detail {

template<typename T>
UTTER_INLINE constexpr iovec to_iovec(T&& buf) {
    return {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
        .iov_base = const_cast<char*>(buf.data),
        .iov_len = buf.len,
    };
}

template<typename... Ts>
UTTER_INLINE ssize_t to(int file, Ts&&... bufs) {
    const iovec vecs[] = {
        ::utter::detail::to_iovec(bufs)...,
    };
    return ::writev(file, vecs, sizeof...(Ts));
}

} // namespace utter::detail

namespace utter {

static constexpr int STDOUT = 1;
static constexpr int STDERR = 2;

template<typename... Args>
UTTER_INLINE ssize_t to(int file, Args&&... args) {
    // need a wrapper to keep the buffers alive for the whole duration
    // of the function
    return ::utter::detail::to(file, ::utter::to_buffer(args)...);
}

} // namespace utter

#undef UTTER_INLINE

#endif
