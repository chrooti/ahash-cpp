# aHash

Cpp implementation of [aHash](https://github.com/tkaitchuck/aHash).

### How to

```C
#include <ahash.hpp>
```
Look for everything under the `/* PUBLIC API */` comment to know what you're supposed to use. There are also test, depending on your preference. \
If you want to know why/how the hash works the FAQ under the original repo have some insight.

### Missing features

- Specialized hashes
- Benchmarks
- ASM output check

### Design choices

To keep the code as future proof as possible every identifier was given the same name as the original source,
where this wasn't possible (e.g. reserved word or variables redeclared with different types) the first letter was
repeated, i.e. `xor` becomes `xxor`. \
The code structure also reflects the source to the best of my capabilities, apart from the `read_*` routines, which
were easier to code with offsets.

### Caveats

- Does not support x86 cpus without SSSE3/AES-NI: you're safe if your CPU model was built after 2008 (Intel) or 2011 (AMD).
- Does not support ARM cpus without ARMv8 cryptographic extensions.
- Does not (and will not) support MSVC. Just use clang. Or fork this.
- Linux support is the only one tested. As for other OSes: the code is already there, testers wanted!
- Fallback hash wasn't ported as I'm not interested, there is no technical reason not to port it, though.

### Development

```bash
podman run -it --rm --volume "$(pwd)":/app --workdir /app docker.io/library/rust:alpine /bin/sh
./gen_ninja_config.sh
ninja test
```
