#ifndef AHASH_H
#define AHASH_H

#include <stddef.h>
#include <stdint.h>

#if defined(__x86_64__)
#include <immintrin.h>
#define AHASH_X86_64

#elif defined(__arm__)
#include <arm_neon.h>
#define AHASH_ARM

#else
#error "Unsupported CPU"

#endif

#if defined(__clang__) || defined(__GNUC__)
#define AHASH_INLINE static inline __attribute__((__always_inline__, __unused__))
#define AHASH_ATTRIBUTE(...) __attribute__((__VA_ARGS__))
#define AHASH_LIKELY(x) __builtin_expect(!!(x), 1)
#define AHASH_UNLIKELY(x) __builtin_expect(!!(x), 0)
#define AHASH_BSWAP64 __builtin_bswap64

// this allows _mm_set_epi32 as constexpr
// NOTE: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=78657
#if defined(__x86_64__)
#define AHASH_NEW_UINT64X2(i3, i2, i1, i0) \
    __extension__(__m128i)(__v4si) { i0, i1, i2, i3 }

#elif defined(__arm__)
#define AHASH_NEW_UINT64X2(i3, i2, i1, i0) \
    uint32x4_t { i0, i1, i2, i3 }

#endif

#else
#error "Unsupported compiler"

#endif

namespace ahash {

/* TYPES */

#if defined(AHASH_X86_64)
using uint64x2_t = __m128i;

#elif defined(AHASH_ARM)
using uint64x2_t = ::uint64x2_t;

#endif

struct Hasher {
    uint64x2_t enc;
    uint64x2_t sum;
    uint64x2_t key;
};

struct RandomState {
    uint64_t k0;
    uint64_t k1;
    uint64_t k2;
    uint64_t k3;
};

} // namespace ahash

namespace ahash::detail {

/* CONSTANTS */

static constexpr uint64_t PI[4] = {
    0x243f6a8885a308d3,
    0x13198a2e03707344,
    0xa4093822299f31d0,
    0x082efa98ec4e6c89,
};

static constexpr uint64_t PI2[4] = {
    0x452821e638d01377,
    0xbe5466cf34e90c6c,
    0xc0ac29b7c97c50dd,
    0x3f84d5b5b5470917,
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
static constexpr uint64x2_t SHUFFLE_MASK =
    AHASH_NEW_UINT64X2(0x020a0700, 0x0c01030e, 0x050f0d08, 0x06090b04);
#pragma GCC diagnostic pop

/* INTRINSICS */

#if defined(AHASH_X86_64)

AHASH_INLINE uint64x2_t new_uint64x2_zero() {
    return _mm_setzero_si128();
}

AHASH_INLINE uint64x2_t new_uint64x2_1(uint64_t q) {
    return _mm_set1_epi64x(static_cast<long long>(q));
}

AHASH_INLINE uint64x2_t new_uint64x2(uint64_t q1, uint64_t q0) {
    return _mm_set_epi64x(static_cast<long long>(q1), static_cast<long long>(q0));
}

AHASH_INLINE uint64x2_t add_by_64s(uint64x2_t a, uint64x2_t b) {
    return _mm_add_epi64(a, b);
}

AHASH_INLINE void add_in_length(uint64x2_t* enc, size_t llen) {
    const uint64x2_t len = _mm_cvtsi64_si128(static_cast<long long>(llen));
    const uint64x2_t data = _mm_loadu_si128(enc);
    const uint64x2_t sum = _mm_add_epi64(data, len);
    _mm_storeu_si128(enc, sum);
}

AHASH_INLINE uint64x2_t shuffle(uint64x2_t a) {
    return _mm_shuffle_epi8(a, SHUFFLE_MASK);
}

AHASH_INLINE uint64x2_t aesenc(uint64x2_t value, uint64x2_t xxor) {
    return _mm_aesenc_si128(value, xxor);
}

AHASH_INLINE uint64x2_t aesdec(uint64x2_t value, uint64x2_t xxor) {
    return _mm_aesdec_si128(value, xxor);
}

#elif defined(AHASH_ARM)

AHASH_INLINE uint64x2_t new_uint64x2_zero() {
    return {0, 0};
}

AHASH_INLINE uint64x2_t new_uint64x2_1(uint64_t q) {
    return {q, q};
}

AHASH_INLINE uint64x2_t new_uint64x2(uint64_t q1, uint64_t q0) {
    return {q0, q1};
}

AHASH_INLINE uint64x2_t add_by_64s(uint64x2_t a, uint64x2_t b) {
    return vaddq_u64(a, b);
}

AHASH_INLINE void add_in_length(uint64x2_t* enc, size_t len) {
    enc[0] += len;
}

AHASH_INLINE uint64x2_t shuffle(uint64x2_t a) {
    return {
        AHASH_BSWAP64(a[0]),
        AHASH_BSWAP64(a[1]),
    };
}

AHASH_INLINE uint64x2_t aesenc(uint64x2_t value, uint64x2_t xxor) {
    return vaesmcq_u8(vaeseq_u8(value, xxor));
}

AHASH_INLINE uint64x2_t aesdec(uint64x2_t value, uint64x2_t xxor) {
    return vaesimcq_u8(vaesdq_u8(value, xxor));
}

#endif

AHASH_INLINE uint64x2_t shuffle_and_add(uint64x2_t base, uint64x2_t to_add) {
    const uint64x2_t shuffled = shuffle(base);
    return add_by_64s(shuffled, to_add);
}

/* IO */

AHASH_INLINE uint32_t read_last_u32(const char* data, size_t data_length) {
    return *reinterpret_cast<const uint32_t*>(data + data_length - 4);
}

AHASH_INLINE uint64_t read_last_u64(const char* data, size_t data_length) {
    return *reinterpret_cast<const uint64_t*>(data + data_length - 8);
}

AHASH_INLINE uint64x2_t read_last_u128(const char* data, size_t data_length) {
    return *reinterpret_cast<const uint64x2_t*>(data + data_length - 16);
}

AHASH_INLINE const uint64x2_t* read_last_u128x2(const char* data, size_t data_length) {
    return reinterpret_cast<const uint64x2_t*>(data + data_length - 32);
}

AHASH_INLINE const uint64x2_t* read_last_u128x4(const char* data, size_t data_length) {
    return reinterpret_cast<const uint64x2_t*>(data + data_length - 64);
}

AHASH_INLINE uint64x2_t read_small(const char* data, size_t data_length) {
    if (data_length >= 2) {
        if (data_length >= 4) {
            // len 4-8
            return detail::new_uint64x2(
                read_last_u32(data, data_length),
                reinterpret_cast<const uint32_t*>(data)[0]
            );
        } else {
            // len 2-3
            return detail::new_uint64x2(
                static_cast<uint64_t>(data[data_length - 1]),
                reinterpret_cast<const uint16_t*>(data)[0]
            );
        }
    } else {
        if (data_length > 0) {
            return new_uint64x2_1(static_cast<uint64_t>(data[0]));
        } else {
            return new_uint64x2_zero();
        }
    }
}

/* HASHING */

AHASH_INLINE void hash_in(Hasher& hasher, uint64x2_t new_value) {
    hasher.enc = aesenc(hasher.enc, new_value);
    hasher.sum = shuffle_and_add(hasher.sum, new_value);
}

AHASH_INLINE void hash_in_2(Hasher& hasher, uint64x2_t v1, uint64x2_t v2) {
    hasher.enc = aesenc(hasher.enc, v1);
    hasher.sum = shuffle_and_add(hasher.sum, v1);
    hasher.enc = aesenc(hasher.enc, v2);
    hasher.sum = shuffle_and_add(hasher.sum, v2);
}

} // namespace ahash::detail

namespace ahash {

/* PUBLIC API */

AHASH_INLINE Hasher new_with_keys(uint64x2_t key1, uint64x2_t key2) {
    const auto* pi = reinterpret_cast<const uint64x2_t*>(detail::PI);
    key1 = key1 ^ pi[0];
    key2 = key2 ^ pi[1];

    return {
        .enc = key1,
        .sum = key2,
        .key = key1 ^ key2,
    };
}

AHASH_INLINE Hasher test_with_keys(uint64x2_t key1, uint64x2_t key2) {
    return {
        .enc = key1,
        .sum = key2,
        .key = key1 ^ key2,
    };
}

AHASH_INLINE Hasher from_random_state(RandomState rand_state) {
    const uint64x2_t key1 = detail::new_uint64x2(rand_state.k1, rand_state.k0);
    const uint64x2_t key2 = detail::new_uint64x2(rand_state.k3, rand_state.k2);

    return {
        .enc = key1,
        .sum = key2,
        .key = key1 ^ key2,
    };
}

AHASH_INLINE void write_u32(Hasher& hasher, uint32_t i) {
    detail::hash_in(hasher, detail::new_uint64x2(0, i));
}

AHASH_INLINE void write_u64(Hasher& hasher, uint64_t i) {
    detail::hash_in(hasher, detail::new_uint64x2(0, i));
}

AHASH_INLINE void write_u128(Hasher& hasher, uint64x2_t i) {
    detail::hash_in(hasher, i);
}

AHASH_ATTRIBUTE(__unused__)
static void write(Hasher& hasher, const char* data, size_t data_length) {
    detail::add_in_length(&hasher.enc, data_length);

    if (data_length <= 8) {
        const uint64x2_t value = detail::read_small(data, data_length);
        detail::hash_in(hasher, value);
    } else {
        if (data_length > 32) {
            if (data_length > 64) {
                const uint64x2_t* const tail =
                    detail::read_last_u128x4(data, data_length);
                uint64x2_t current[4] = {hasher.key, hasher.key, hasher.key, hasher.key};
                current[0] = detail::aesenc(current[0], tail[0]);
                current[1] = detail::aesenc(current[1], tail[1]);
                current[2] = detail::aesenc(current[2], tail[2]);
                current[3] = detail::aesenc(current[3], tail[3]);
                uint64x2_t sum[2] = {hasher.key, hasher.key};
                sum[0] = detail::add_by_64s(sum[0], tail[0]);
                sum[1] = detail::add_by_64s(sum[1], tail[1]);
                sum[0] = detail::shuffle_and_add(sum[0], tail[2]);
                sum[1] = detail::shuffle_and_add(sum[1], tail[3]);
                while (data_length > 64) {
                    const auto* blocks = reinterpret_cast<const uint64x2_t*>(data);
                    current[0] = detail::aesenc(current[0], blocks[0]);
                    current[1] = detail::aesenc(current[1], blocks[1]);
                    current[2] = detail::aesenc(current[2], blocks[2]);
                    current[3] = detail::aesenc(current[3], blocks[3]);
                    sum[0] = detail::shuffle_and_add(sum[0], blocks[0]);
                    sum[1] = detail::shuffle_and_add(sum[1], blocks[1]);
                    sum[0] = detail::shuffle_and_add(sum[0], blocks[2]);
                    sum[1] = detail::shuffle_and_add(sum[1], blocks[3]);

                    data += 64;
                    data_length -= 64;
                }
                detail::hash_in_2(
                    hasher,
                    detail::aesenc(current[0], current[1]),
                    detail::aesenc(current[2], current[3])
                );
                detail::hash_in(hasher, detail::add_by_64s(sum[0], sum[1]));
            } else {
                // len 33-64
                const auto* head = reinterpret_cast<const uint64x2_t*>(data);
                const uint64x2_t* const tail =
                    detail::read_last_u128x2(data, data_length);
                detail::hash_in_2(hasher, head[0], head[1]);
                detail::hash_in_2(hasher, tail[0], tail[1]);
            }
        } else {
            if (data_length > 16) {
                // len 17-32
                detail::hash_in_2(
                    hasher,
                    reinterpret_cast<const uint64x2_t*>(data)[0],
                    detail::read_last_u128(data, data_length)
                );
            } else {
                // len 9-16
                const uint64x2_t value = detail::new_uint64x2(
                    detail::read_last_u64(data, data_length),
                    reinterpret_cast<const unsigned long long*>(data)[0]
                );
                detail::hash_in(hasher, value);
            }
        }
    }
}

AHASH_INLINE uint64_t finish(Hasher& hasher) {
    const uint64x2_t combined = detail::aesdec(hasher.sum, hasher.enc);
    const uint64x2_t result =
        detail::aesenc(detail::aesenc(combined, hasher.key), combined);

    // TODO: (or _mm_extract?)
    // uint64_t retval;
    // __asm__("movq %0, %1" : "=r"(retval) : "x"(result));
    // return retval;
    return static_cast<unsigned long>(result[0]);
}

} // namespace ahash

namespace ahash::random_state {

AHASH_INLINE RandomState with_seeds(uint64_t k0, uint64_t k1, uint64_t k2, uint64_t k3) {
    return {
        k0 ^ detail::PI2[0],
        k1 ^ detail::PI2[1],
        k2 ^ detail::PI2[2],
        k3 ^ detail::PI2[3],
    };
}

} // namespace ahash::random_state

#undef AHASH_X86_64
#undef AHASH_ARM

#undef AHASH_INLINE
#undef AHASH_ATTRIBUTE
#undef AHASH_LIKELY
#undef AHASH_UNLIKELY
#undef AHASH_NEW_UINT64X2

#endif
